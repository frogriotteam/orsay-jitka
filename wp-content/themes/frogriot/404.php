<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }
get_header('home');
?>
<div class="container pnotfound">
    <img data-aos="fade-in" data-aos-duration="2000" data-aos-delay="0" class="pnotfound__bg" src="<?php echo get_template_directory_uri(); ?>/assets/img/404.png" alt="404" />
    <div class="pnotfound__content" data-aos="fade-up" data-aos-delay="600" data-aos-duration="2000">
        <h2 class="pnotfound__header"><?php the_field('404_txt', 'option'); ?></h2>
        <a class="btn btn--orange btn__icon--right" href="<?php echo home_url('/'); ?>"><?php the_field('404_btn_txt', 'option'); ?></a>
    </div>
</div>
<?php get_footer(); ?>