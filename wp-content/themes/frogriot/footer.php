<div class="page-footer">
            <div class="bar-progress"></div>
            <div class="footer-bar">
                <div class="copy">Copyright © Orsay. All right reserved.</div>
                <div class="socials">
                    <a id="facebook_icon" href="https://www.facebook.com/orsay" target="_blank" class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/facebook.svg" alt=""></a>
                    <a id="eshop" href="https://www.orsay.com/cs-cz/?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/bag.svg" alt=""></a>
                </div>
            </div>
        </div>
        <script src="<?php echo get_template_directory_uri(); ?>/dist/app.min.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/slick.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/mobile-detect.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/jquery.onepage-scroll.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/perfect-scrollbar.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/jquery.magnific-popup.js"></script>

        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/plugins/jquery.charactercounter.js"></script>

        <script>

            

            //pixelfb
            $('#contest_weekly_partial').click(function() {
                fbq('track', 'contest_weekly_partial');

                window.dataLayer = window.dataLayer || []
                dataLayer.push({
                'event' : 'RegistrationSuccess',
                customDataFB :
                    {  'content_name' : 'contest_weekly_partial' ,
                       'status' : 'Pending_Email_Verification'
                    }
                })

            });

            $('#contest_main_partial').click(function() {
                fbq('track', 'contest_main_partial');

                window.dataLayer = window.dataLayer || []
                dataLayer.push({
                'event' : 'RegistrationSuccess',
                customDataFB :
                    {  'content_name' : 'contest_main_partial' ,
                       'status' : 'Pending_Email_Verification'
                    }
                })
                
            });

            var md = new MobileDetect(window.navigator.userAgent);

            if (md.mobile() !== null ) {
                $('body').addClass('mobile')
            }    


            //video

            var videoBackstage = $('#video_backstage')[0]
            var videoPlayBtn = $('#video-play')

            $('#video-play').click(function(){
                videoBackstage.play()
                videoBackstage.setAttribute("controls","controls")
                videoPlayBtn.css('display', 'none')
            })

            videoBackstage.onended = function(){
                this.webkitExitFullScreen();
                videoBackstage.currentTime = 0;
                videoPlayBtn.css('display', 'block')
                videoBackstage.removeAttribute("controls", "controls")
                videoBackstage.load();
                
            };

            //mobile menu
            $('#mobile-menu-btn').click(function(){
                $('body').addClass('menu-open')
            })

            $('#mobile-menu-close').click(function(){
                $('body').removeClass('menu-open')
            })

            $("#mobile-menu a").click(function(){
                $('body').removeClass('menu-open')
            })

            // Add smooth scrolling to all links
            $("a").not('.btn-action, .btn-popup').on('click', function(event) {

                if (this.hash !== "") {
                    event.preventDefault();

                    var hash = this.hash;

                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function(){

                        window.location.hash = hash;
                    });
                } 
            });

            if ($(window).width() < 1199) {
                $(".btn-action").on('click', function(event) {

                    if (this.hash !== "") {
                        event.preventDefault();

                        var hash = this.hash;

                        $('html, body').animate({
                            scrollTop: $(hash).offset().top
                        }, 800, function(){

                            window.location.hash = hash;
                        });
                    } 
                    });
            }


            $("#fullpage").onepage_scroll({
                updateURL: true,
                responsiveFallback: 1199,
                afterMove: function(index) {
                    barHighlight(index)

                    if(index == 1 || index == 6) {
                        $('.catalog-slider-container').removeClass('collapsed')
                    }

                    var page_footer = $('.page-footer')
                    page_footer.addClass('active')
                },
                beforeMove: function(index) {
                    
                    var page_footer = $('.page-footer')
                    page_footer.removeClass('active')
                }
            });

            if($(window).width() > 1199) {
                //set progress-bar
                $("#fullpage").moveTo(1);
            }

            function goPage(e, page) {
                if($(window).width() > 1199) {
                    e.preventDefault();
                    $("#fullpage").moveTo(page);
                } else {
                    return
                }
            };

            //product slider, catalog slider
            $('.product-slider').slick({
                vertical: true,
                slidesToShow: 2,
                responsive: [
                    {
                    breakpoint: 767,
                        
                        settings: {
                            vertical: false,
                            slidesToShow: 1,
                        }
                    },

                ]
            });

            $('.catalog-slider-all').slick({
                slidesToShow: 3,
                responsive: [
                    {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                    }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                        slidesToShow: 1,
                        }
                    },
                ]
            });

            $(window).on('load', function() {
                $('body').addClass('loaded')

                var promise = document.querySelector('#startVideo').play();

                if (promise !== undefined) {
                    promise.then(_ => {
                        // Autoplay started!
                    }).catch(error => {
                        // Autoplay was prevented.
                        // Show a "Play" button so that user can start playback.
                    });
                }
            })


            //showing/hiding main menu
            var winW = $(window).width();

            if (winW < 1440) {
                $('.onepage-pagination').mouseenter(function(){
                    $(this).addClass('visible')
                });
                $('.onepage-pagination').mouseleave(function(){
                    $(this).removeClass('visible')
                })
            }

            function barHighlight(index) {
                var items = $('.bar-progress .item')
                var bar = $('.bar-progress')

                items.each(function(index, item){
                    $(item).removeClass('done')
                })
                
                for (var i = 0; i < index; i++) {
                    $(items).eq(i).addClass('done')
                }

                var paddingWidth = 100/items.length

               /*  bar.css('padding-right', paddingWidth + '%') */
            }

            var menu_links = $('.onepage-pagination').find('li')

            var linkNames = [
                'Start',
                'O AKCI',
                'JITKA NOVÁČKOVÁ',
                'KAMPAŇOVÉ VIDEO',
                'MASTERCARD®',
                'SOUTĚŽIT',
                //'TÝDENNÍ SOUTĚŽ',
                'VÍTĚZKY',
                //'výsledky'
            ]

            menu_links.each(function(index, link){
                $(link).find('a').prepend(linkNames[index])
            })

            

            //progress bar
            var sections_summ = $('#fullpage .section').length
            var bar = $('.bar-progress')
            var html = '';
        
            for (var i = 1; i <= sections_summ + 1; i++) {
                html += `<div class="item page-`+ i +`">
                        <div class="inner"></div>
                    </div>`
            }

            bar.append(html);

            

        </script>


        <!--------------------contest------------------------>

        <script>

            Array.prototype.remove = function() {
                var what, a = arguments, L = a.length, ax;
                while (L && this.length) {
                    what = a[--L];
                    while ((ax = this.indexOf(what)) !== -1) {
                        this.splice(ax, 1);
                    }
                }
                return this;
            };

            var clothesSummary = [];
            var items_gora = [],
                items_dol  = [],
                items_dod  = [];

            $(document).ready(function(){

                $('.closemagnific').on( "click", function(e) {
                    e.preventDefault();
                    $.magnificPopup.close();
                });

                setTimeout(function(){
                    ga('send','event','Profitable Engagement', 'time on page more than 1 minute')
                    gtag('event', 'Profitable Engagement');
                },60000);

                $(document).on('keypress', 'input.error, #description', function(){
                    $(this).removeClass('error');
                    $(this).next('div').hide();
                })

                $(document).on('change', 'input[type="checkbox"]', function(){
                    $(this).next('span').removeClass('error');
                    $('#agree-error, #agree2-error').hide();
                })

                var description_text = $('#description');
                if( description_text.length == 1 ){
                    description_text.inputlimiter({
                        limit: 1000,
                        remText: 'Zbývá %n znaků',
                        limitText: ''
                    });
                }

                $('.slider-photo').on('click', function(e){
                    gtag('event', 'slider_photo');
                });

                $('#eshop').on('click', function(){
                    gtag('event', 'eshop_footer_icon');
                });

                $('#video_backstage').on('play', function(){
                    gtag('event', 'backstage_video');
                })

                $('.backstage_photo').on('click', function(){
                    gtag('event', 'backstage_photo');
                });

                $('#facebook_icon').on('click', function(){
                    gtag('event', 'facebook_icon');
                });

                $('#mastercard_specials').on('click', function(){
                    gtag('event', 'mastercard_specials');
                });

                $('#section3 .slick-prev').on('click', function(){
                    gtag('event', 'slider__prev');
                });

                $('#section3 .slick-next').on('click', function(){
                    gtag('event', 'slider__next');
                });

                $('#section4 .slick-next').on('click', function(){
                    gtag('event', 'backstage__next');
                });

            });

            function goTo(step_num, contest_kind) {

                var target = (contest_kind == 'glowny') ? $('#contest-popup') : $('#contest-popup2')

                var body = $("html, body");
                    body.stop().animate({scrollTop:target.offset().top}, 300);

                if (step_num == 2 && contest_kind == 'glowny') {

                    var nonce = $('body').attr('data-nonce'),
                        ajax_url = $('body').attr('data-ajax'),
                        email = $('#email').val(),
                        flname = $('#flname').val(),
                        agree = 0,
                        newsletter = 0;

                    if( $('#agree').prop('checked') == true){
                        agree = 1;
                    }

                    if( $('#newsletter').prop('checked') == true){
                        newsletter = 1;
                    }

                    var dataString = {
                        action: 'process_step_one',
                        email: email,
                        flname: flname,
                        agree: agree,
                        newsletter: newsletter,
                        security: nonce
                    };

                    var btn = $('#goToStep2');
                    btn.prop('disabled', true);
                    

                    if( $('#contest .step-1').hasClass('done') ){
                        var steps = $('#contest .step');
                        steps.each(function(index, step){
                            $(step).removeClass('active')
                            $('#contest .step-'+step_num).addClass('active')
                        })

                        $('#contest').prop('class', 'content-layout')
                        $('#contest').addClass('step-'+step_num)
                        //go form first step
                        $('#contest').removeClass('start-step');
                        
                    }else{
                        /* $.ajax({
                            url: ajax_url,
                            data: dataString,
                            type: 'POST',
                            success: function (data) {

                                $('#agree').next('span').removeClass('error');

                                btn.prop('disabled', false);
                                
                                if (data.passed == true) {

                                    var steps = $('#contest .step');
                                    steps.each(function(index, step){
                                        $(step).removeClass('active')
                                        $('#contest .step-'+step_num).addClass('active')
                                    })

                                    $('#contest').prop('class', 'content-layout')
                                    $('#contest').addClass('step-'+step_num)
                                    //go form first step
                                    $('#contest').removeClass('start-step');

                                    $('#contest .step-1').addClass('done');

                                    gtag('event', 'signup_main_partial');

                                    new PerfectScrollbar('#odziez_gora');

                                }else{
                                    $('#goToStep2msg').text('Formulář obsahuje chyby').fadeIn(300);
                                    if( data.errors == true ){
                                        $.each(data.errormsgs, function (key, val) {
                                            if( key == 'email' && key != '' ){
                                                $('#email').addClass('error');
                                                $('#email-error').text(val).fadeIn(300);
                                            }else if( key == 'flname' && key != '' ){
                                                $('#flname').addClass('error');
                                                $('#flname-error').text(val).fadeIn(300);
                                            }else if( key == 'agree' && val != '' ){
                                                $('#agree').next('span').addClass('error');
                                                $('#agree-error').text(val).fadeIn(300);
                                            }
                                        });
                                    } 
                                }

                            },
                            error: function (error) {
                                btn.prop('disabled', false);
                                console.log('error');
                            }
                        });
                        return false; */
                    }
                    
                }

                if (step_num == 3 && contest_kind == 'glowny') {

                    

                    if( slider_gora.slick("getSlick").slideCount === 1 ){

                        var steps = $('#contest .step');
                        steps.each(function(index, step){
                            $(step).removeClass('active')
                            $('#contest .step-'+step_num).addClass('active')
                        })
                        $('#contest').prop('class', 'content-layout')
                        $('#contest').addClass('step-'+step_num)
                        //go form first step
                        $('#contest').removeClass('start-step'); 
                        $('#goToStep3msg').hide();
                        new PerfectScrollbar('#odziez_dol');
                    }else{
                        $('#goToStep3msg').text('Vyberte si oblečení kliknutím na obrázky.').fadeIn(300);
                    }

                }
                
                if (step_num == 4 && contest_kind == 'glowny') {
                    

                    if( slider_dol.slick("getSlick").slideCount === 1 ){
                        var steps = $('#contest .step');
                        steps.each(function(index, step){
                            $(step).removeClass('active')
                            $('#contest .step-'+step_num).addClass('active')
                        })
                        $('#contest').prop('class', 'content-layout')
                        $('#contest').addClass('step-'+step_num)
                        //go form first step
                        $('#contest').removeClass('start-step'); 
                        $('#goToStep4msg').hide();
                        new PerfectScrollbar('#dodatki');
                    }else{
                        $('#goToStep4msg').text('Vyberte si oblečení kliknutím na obrázky').fadeIn(300);
                    }
                }

                if (step_num == 5 && contest_kind == 'glowny') {

                    summary_slider.slick('slickRemove', null, null, true);

                    if( slider_dodatki.slick("getSlick").slideCount > 0 && slider_dodatki.slick("getSlick").slideCount < 4 ){
                        var steps = $('#contest .step');
                        steps.each(function(index, step){
                            $(step).removeClass('active')
                            $('#contest .step-'+step_num).addClass('active')
                        })
                        $('#contest').prop('class', 'content-layout')
                        $('#contest').addClass('step-'+step_num)
                        //go form first step
                        $('#contest').removeClass('start-step'); 
                        $('#goToStep5msg').hide();
                    }else{
                        $('#goToStep5msg').text('Vyberte si příslušenství kliknutím na obrázky (max. 3 příslušenství)').fadeIn(300);
                    }


                    //summary
                    $.each( clothesSummary, function(index, src){
                        var html = `<div>
                                    <div class="image">
                                        <img src="`+src+`" alt="">
                                    </div>
                                </div>
                                `
                        summary_slider.slick('slickAdd', html);               
                    })

                }

                if(step_num == 6 && contest_kind == 'glowny'){
                    
                    var nonce = $('body').attr('data-nonce'),
                        ajax_url = $('body').attr('data-ajax'),
                        flname = $('#flname').val(),
                        email = $('#email').val(),
                        items_gora = $('#items_gora').val(),
                        items_dol = $('#items_dol').val(),
                        items_dod = $('#items_dod').val();


                    var dataString = {
                        action: 'process_step_final',
                        items_gora: items_gora,
                        items_dol: items_dol,
                        items_dod: items_dod,
                        flname: flname,
                        email: email,
                        security: nonce
                    };

                    var btn = $('#goToStep6');
                    btn.prop('disabled', true);

                    /* $.ajax({
                        url: ajax_url,
                        data: dataString,
                        type: 'POST',
                        success: function (data) {

                            btn.prop('disabled', false);

                            if( data.passed == true){
                                var steps = $('#contest .step');
                                    steps.each(function(index, step){
                                        $(step).removeClass('active')
                                        $('#contest .step-'+step_num).addClass('active')
                                    })
                                    $('#contest').prop('class', 'content-layout')
                                    $('#contest').addClass('step-'+step_num)
                                    //go form first step
                                    $('#contest').removeClass('start-step');
                                    gtag('event', 'signup_main_full');

                            }else{
                                $('#goToStep6msg').text('Formulář obsahuje chyby').fadeIn(300);
                            }
                            

                        },
                        error: function (error) {
                            btn.prop('disabled', false);
                        }
                    });

                    return false; */
                    
                }

                //konkurs tygodniowy

                if (step_num == 2 && contest_kind == 'tygodniowy') {

                    var nonce = $('body').attr('data-nonce'),
                        ajax_url = $('body').attr('data-ajax'),
                        email = $('#email2').val(),
                        flname = $('#flname2').val(),
                        agree = 0,
                        newsletter = 0;

                    if( $('#agree2').prop('checked') == true){
                        agree = 1;
                    }

                    if( $('#newsletter2').prop('checked') == true){
                        newsletter = 1;
                    }

                    var dataString = {
                        action: 'process_weekly',
                        email: email,
                        flname: flname,
                        agree: agree,
                        newsletter: newsletter,
                        security: nonce
                    };

                    var btn = $('#goToStep2-tygodniowy');
                    btn.prop('disabled', true);

                    /* $.ajax({
                        url: ajax_url,
                        data: dataString,
                        type: 'POST',
                        success: function (data) {

                            $('#agree2').next('span').removeClass('error');
                            btn.prop('disabled', false);
                            if (data.passed == true) {

                                var steps = $('#contest-tygodniowy .step');
                                steps.each(function(index, step){
                                    $(step).removeClass('active')
                                    $('#contest-tygodniowy .step-'+step_num).addClass('active')
                                })

                                $('#contest-tygodniowy').prop('class', 'content-layout')
                                $('#contest-tygodniowy').addClass('step-'+step_num)
                                //go form first step
                                $('#contest-tygodniowy').removeClass('start-step');
                                $('#contest-tygodniowy .step-1').addClass('done');

                                gtag('event', 'signup_weekly_partial');

                            }else{
                                $('#goToStep2msg_tygodniowy').text('Formulář obsahuje chyby').fadeIn(300);
                                $('#agree2').next('span').removeClass('error');
                                if( data.errors == true ){
                                    $.each(data.errormsgs, function (key, val) {
                                        if( key == 'email' && key != '' ){
                                            $('#email2').addClass('error');
                                            $('#email2-error').text(val).fadeIn(300);
                                        }else if( key == 'flname' && key != '' ){
                                            $('#flname2').addClass('error');
                                            $('#flname2-error').text(val).fadeIn(300);
                                        }else if( key == 'agree' && val != '' ){
                                            $('#agree2').next('span').addClass('error');
                                            $('#agree2-error').text(val).fadeIn(300);
                                        }
                                    });
                                } 
                            }

                        },
                        error: function (error) {
                            btn.prop('disabled', false);
                            console.log('error');
                        }
                    });
                    return false; */

                }

                if (step_num == 3 && contest_kind == 'tygodniowy') {

                    var nonce = $('body').attr('data-nonce'),
                        ajax_url = $('body').attr('data-ajax'),
                        email = $('#email2').val(),
                        description = $('#description').val();


                    var dataString = {
                        action: 'process_weekly_final',
                        email: email,
                        description: description,
                        security: nonce
                    };

                    var btn = $('#goToStep6');
                    btn.prop('disabled', true);

                    /* $.ajax({
                        url: ajax_url,
                        data: dataString,
                        type: 'POST',
                        success: function (data) {

                            btn.prop('disabled', false);

                            if( data.passed == true){

                                var steps = $('#contest-tygodniowy .step');
                                steps.each(function(index, step){
                                    $(step).removeClass('active');
                                    $('#contest-tygodniowy .step-'+step_num).addClass('active');
                                })
                                $('#contest-tygodniowy').prop('class', 'content-layout');
                                $('#contest-tygodniowy').addClass('step-'+step_num);

                                gtag('event', 'signup_weekly_full');

                            }else{
                                $('#goToStep3msg_tygodniowy').text('Formulář obsahuje chyby').fadeIn(300);
                                if( data.errors == true ){
                                    $.each(data.errormsgs, function (key, val) {
                                        if( key == 'description' && key != '' ){
                                            $('#description').addClass('error');
                                            $('#description-error').text(val).fadeIn(300);
                                        }
                                    });
                                } 
                            }
                            

                        },
                        error: function (error) {
                            btn.prop('disabled', false);
                        }
                    }); */

                    return false;

                }
                
            }
            

            //slider odzież góra

            var slider_gora = $('#odziez_gora-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false
            });


            $('#odziez_gora').find('.image').click(function(){
                $('#goToStep3msg').hide();
                var src = $(this).find('img').data('src'),
                    img_id = $(this).find('img').attr('id');

                var html = `<div>
                            <div class="image">
                                <img id="`+img_id+`" src="`+src+`" alt="">
                            </div>
                            <div data-src="`+src+`" class="remove">Smazat</div>
                            </div>
                            `
                if( slider_gora.slick("getSlick").slideCount === 0 ){
                    $('#odziez_gora-slider').slick('slickAdd', html);
                    clothesSummary.push(src)
                    slider_gora.slick('slickNext')
                    $('#items_gora').val(img_id);
                }

                
                
                var target = $('#odziez_gora-wybrane')

                var body = $("html, body");
                    body.stop().animate({scrollTop:target.offset().top}, 300);

                if( slider_gora.slick("getSlick").slideCount >= 1 ){
                    $('#odziez_gora-wybrane .info').css('display', 'none')
                } 

            })

            slider_gora.on('click', '.remove', function() {
                var slide_index = $(this).parents('.slick-slide').data('slick-index')
                slider_gora.slick('slickRemove', slide_index);
                var j = 0;
                slider_gora.find(".slick-slide").each(function(){
                    $(this).attr("data-slick-index",j);
                    j++;
                });
                clothesSummary.remove($(this).data('src'));
                $('#items_gora').val('');

                if( slider_gora.slick("getSlick").slideCount == 0 ){
                    $('#odziez_gora-wybrane .info').css('display', 'block')
                } 
            });

            //slider odzież dół

            var slider_dol = $('#odziez_dol-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false,
            });

            $('#odziez_dol').find('.image').click(function(){
                $('#goToStep4msg').hide();
                var src = $(this).find('img').data('src'),
                    img_id = $(this).find('img').attr('id');

                var html = `<div>
                            <div class="image">
                                <img src="`+src+`" alt="">
                            </div>
                            <div data-src="`+src+`" data-id="`+img_id+`" class="remove">Smazat</div>
                            </div>
                            `

                if( slider_dol.slick("getSlick").slideCount === 0 ){
                    slider_dol.slick('slickAdd', html);
                    clothesSummary.push(src)
                    slider_dol.slick('slickNext')
                    $('#items_dol').val(img_id);
                }
                
                var target = $('#odziez_dol-wybrane')

                var body = $("html, body");
                    body.stop().animate({scrollTop:target.offset().top}, 300);


                if( slider_dol.slick("getSlick").slideCount >= 1 ){
                    $('#odziez_dol-wybrane .info').css('display', 'none')
                } 
            })

            slider_dol.on('click', '.remove', function() {
                var slide_index = $(this).parents('.slick-slide').data('slick-index')
                slider_dol.slick('slickRemove', slide_index);
                var j = 0;
                slider_dol.find(".slick-slide").each(function(){
                    $(this).attr("data-slick-index",j);
                    j++;
                });
                clothesSummary.remove($(this).data('src'));

                $('#items_dol').val('');

                if( slider_dol.slick("getSlick").slideCount == 0 ){
                    $('#odziez_dol-wybrane .info').css('display', 'block')
                } 
            });

            //slider dodatki

            if ($(window).width() > 767) {
                var slider_dodatki = $('#odziez_dodatki-slider').slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
                });
            }

            if ($(window).width() < 768) {
                var slider_dodatki = $('#odziez_dodatki-slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                });
            }

            

            $('#dodatki').find('.image').click(function(){
                
                var src = $(this).find('img').data('src'),
                    img_id = $(this).find('img').attr('id');

                var html = `<div>
                            <div class="image">
                                <img data-src="`+src+`" src="`+src+`" alt="">
                            </div>
                            <div data-src="`+src+`" data-id="`+img_id+`" class="remove">Smazat</div>
                            </div>
                            `

                if( slider_dodatki.slick("getSlick").slideCount < 3 ){
                    slider_dodatki.slick('slickAdd', html);
                    clothesSummary.push(src)
                    slider_dodatki.slick('slickNext')
                    items_dod.indexOf(img_id) === -1 ? items_dod.push(img_id) : console.log("This item already exists");
                    $('#items_dod').val(items_dod.join());
                }

                var target = $('#dodatki_wybrane')

                var body = $("html, body");
                    body.stop().animate({scrollTop:target.offset().top}, 300);

                if( slider_dodatki.slick("getSlick").slideCount >= 1 ){
                    $('#dodatki_wybrane .info').css('display', 'none')
                } 

                
            })

            slider_dodatki.on('click', '.remove', function() {
                var item = $(this).data('id');
                var slide_index = $(this).parents('.slick-slide').data('slick-index')
                slider_dodatki.slick('slickRemove', slide_index);
                var j = 0;
                slider_dodatki.find(".slick-slide").each(function(){
                    $(this).attr("data-slick-index",j);
                    j++;
                });
 
                clothesSummary.remove($(this).data('src'));
                items_dod.remove($(this).data('id'));

                $('#items_dod').val(items_dod.join());

                if( slider_dodatki.slick("getSlick").slideCount == 0 ){
                    $('#dodatki_wybrane .info').css('display', 'block')
                }
            });


            //summary

            if ($(window).width() > 767) {
                var summary_slider = $("#odziez_summary").slick({
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
                });
            }

            if ($(window).width() < 768) {
                var summary_slider = $("#odziez_summary").slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                });
            }

            //contest popup
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',

                fixedContentPos: false,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,
                
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in',
            });

        
    </script>
    </body>
</html>