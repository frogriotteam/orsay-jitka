const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    entry: {
        app: './entry.js',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].min.js',
    },
    module:{
        rules:[
            {
                test: /\.s?[ac]ss$/,
                use: [
                  MiniCssExtractPlugin.loader,
                  'css-loader',
                  'postcss-loader',
                  'sass-loader',
                ],
            },
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env'],
                  }
                }
            },
            {test: /\.png|jpg|gif$/, loader: 'file-loader' },
            {test: /\.svg|woff|woff2|ttf|eot|otf(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
          // Options similar to the same options in webpackOptions.output
          // both options are optional
          filename: '[name].min.css',
        }),
        new LiveReloadPlugin(),
    ],
    watch:true,
    devtool: 'source-map',
};