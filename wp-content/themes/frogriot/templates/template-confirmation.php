<?php
/*
Template Name: Confirmation
*/
if ( ! defined( 'ABSPATH' ) ) { exit; }

get_header();

if( isset($_GET['email']) && isset($_GET['contest']) ){

    $contest = $_GET['contest'];
    $email = $_GET['email'];
    $table = '';

    global $wpdb;

    if( $contest == 1 ){
        $table = $wpdb->prefix."zgloszenia";
    }elseif( $contest == 2 ){
        $table = $wpdb->prefix."zgloszenia_tygodniowy";
    }

    $results = $wpdb->get_results("SELECT * FROM $table WHERE email = '$email' LIMIT 1" ,ARRAY_A);

    if( count( $results ) > 0 ){
        foreach($results as $res){

            if( $res['confirmation'] == 0 ){

                $date = date('Y-m-d H:i:s');

                $update = $wpdb->update( 
                    $table, 
                    array(
                        'confirmation' => 1,
                        'confirmation_date' => $date
                    ),
                    array(
                        'email' => $email
                    )
                );

                ?>

                <div class="page-typ">
                    <header id="page-header" style="transform: none; position: static">
                        <div class="container">
                            <div class="logo">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/orsay_logo_black.svg" alt="">
                            </div>
                            <div class="logo-mastercard">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mastercard_priceless_black.png" alt="">
                            </div>
                        </div>
                    </header>

                    <div class="page-typ-content">
                        
                        <h1>Děkujeme! </h1>

                        <p>Tvoje přihláška byla zaregistrována.</p>

                        <a href="<?php echo home_url(); ?>" class="btn">PŘEJÍT NA STRÁNKU </a>

                    </div>

                    <div class="page-footer">
                        <div class="footer-bar">
                            <div class="copy">Copyright © Orsay. All right reserved.</div>
                            <div class="socials">
                                <a href="https://www.facebook.com/orsaypl/" target="_blank" class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/facebook.svg" alt=""></a>
                                <a href="https://www.orsay.com/pl-pl/" target="_blank" class="item"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/bag.svg" alt=""></a>
                            </div>
                        </div>
                    </div>

                </div>
                <?php  get_footer(); ?>

                <?php
                if( isset($_GET['email']) && isset($_GET['contest']) && $_GET['contest'] == 1 ){
                ?>
                <script>
                    $(document).ready(function(){
                        gtag('event', 'confirmation_main');

                        fbq('track', 'contest_main_full');

                        window.dataLayer = window.dataLayer || []
                        dataLayer.push({
                        'event' : 'RegistrationSuccess',
                        customDataFB :
                            {  'content_name' : 'contest_main_full' ,
                               'status' : 'full_verification'
                            }
                        })
                    });
                </script>
                <?php
                }elseif( isset($_GET['email']) && isset($_GET['contest']) && $_GET['contest'] == 2 ){
                ?>
                <script>
                    $(document).ready(function(){
                        gtag('event', 'confirmation_weekly');

                        fbq('track', 'contest_weekly_full');

                        window.dataLayer = window.dataLayer || []
                        dataLayer.push({
                        'event' : 'RegistrationSuccess',
                        customDataFB :
                            {  'content_name' : 'contest_weekly_full' ,
                               'status' : 'full_verification'
                            }
                        })
                    });
                    
                </script>
                <?php
                }
                ?>
            <?php
            }else{
                wp_redirect('/');
                exit();
            }
        }
    }else{
        wp_redirect('/');
        exit();
    }

}else{
    wp_redirect('/');
    exit();
}
?>