<?php
/*
Template Name: Home
*/
if ( ! defined( 'ABSPATH' ) ) { exit; }
get_header();

if ( have_posts() ) :
while ( have_posts() ) : the_post();
?>

<div class="preloader">
    <div class="lds-roller">
        <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
    </div>
</div>
<div id="contest-popup" class="zoom-anim-dialog mfp-hide">
    <div class="section contest-section">
        <div class="container">
            <div class="content-layout start-step" id="contest">
                <div class="desc-col active">
                    <h2 class="header-border">Hlavní soutěž</h2>
                    <h2 class="header-level-1">
                        UKAŽTE NÁM, V JAKÉM OBLEČENÍ<br>DOBUDETE SVĚT BUSINESSU.
                    </h2>
                    <p>
                        Vytvořte si v našem generátoru outfitů ten vysněný business look a vyhrajte poukázku 
                        v hodnotě <b>25 000 CZK</b> na nákupy v Orsay!
                    </p>
                </div>
                <div class="form-col">
                    <form class="form">
                        <div class="step step-1 active">
                            <div class="content">
                                <h3 class="header-level-4">Krok 1</h3>
                                <div class="controls">
                                    <label class="control-box">
                                        <span class="label-txt">Jméno a příjmení</span>
                                        <input id="flname" type="text" class="control-form" name="flname">
                                        <div id="flname-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <span class="label-txt">Email</span>
                                        <input id="email" type="email" class="control-form" name="email">
                                        <div id="email-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <div class="checkbox-ctn">
                                            <input id="agree" type="checkbox" class="control-form" name="agree">
                                            <span class="state"></span>
                                        </div>
                                        <span class="label-txt label-checkbox">
                                        POTVRZUJI, ŽE JSEM SE SEZNÁMIL/A S PRAVIDLY A SOUHLASÍM S JEJICH ZNĚNÍ. PROHLAŠUJI, ŽE JSEM SE SEZNÁMIL/A S POLITIKOU OCHRANY OSOBNÍCH ÚDAJŮ
                                        </span>
                                        <div id="agree-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <div class="checkbox-ctn">
                                            <input id="newsletter" type="checkbox" class="control-form" name="newsletter">
                                            <span class="state"></span>
                                        </div>
                                        <span class="label-txt label-checkbox">Chci se přihlásit k odběru novinek, a proto souhlasím s tím, že budu zasílat marketingový obsah (obchodní informace) na svou e-mailovou adresu o nových produktech, spolupráci a událostech ve značce Orsay.</span>
                                    </label>
                                    <a href="<?php echo get_template_directory_uri(); ?>/assets/soutez_orsay.pdf" class="get-regulations" download>Pravidla a podmínky</a>
                                </div>
                                <button id="goToStep2" type="button" class="btn-form" onclick="goTo(2, 'glowny')"><span>Dále</span></button>
                                <div class="form-error-msg" id="goToStep2msg"></div>
                                <div class="form-foot">
                                    Záleží nám na Tvém soukromí. Správcem osobních údajů poskytnutých ve formuláři bude společnost ORSAY GmbH, Sitz der Gesell: Willstaett, Německo, zapsána v soudním rejstříku Amtsegericht Freiburg HRB 370336, zastoupena v Polsku přes Ordipol Sp. z o. o. , ul. Logistyczna 1, 50-040 Bielany Wrocławskie.
                                    Poskytnuté údaje budou zpracovány pro účely uvedené v pravidlech soutěže, prosíme Tě tedy o jejich přečtení. Podrobnosti týkající se zpracování osobních údajů
                                </div>
                            </div>
                        </div>
                        <div class="step step-2">
                            <div class="inner content">
                                <div class="col-left">
                                    <h3 class="header-level-4 header-mobile">Krok 2</h3>
                                    <div class="header">Zvolte vrchní část</div>
                                    <div class="images" id="odziez_gora">
                                        <div class="image">
                                            <img id="gora-1" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/105041_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/105041_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/107041_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/107041_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-3" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/125063_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/125063_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/130067_96p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/130067_96p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150169_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150169_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-6" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150178_95p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150178_95p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-7" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150179_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/150179_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-8" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/155036_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/155036_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-9" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/155036_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/155036_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-10" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480202_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480202_98p.jpg">
                                        </div>


                                        <div class="image">
                                            <img id="gora-11" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480203_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480203_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-12" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480204_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480204_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-13" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480205_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480205_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-14" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480206_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480206_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-15" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480207_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480207_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-16" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480209_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480209_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-17" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480210_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480210_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-18" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480211_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480211_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-19" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480213_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480213_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-20" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480214_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/480214_98p.jpg">
                                        </div>
                                        

                                        <div class="image">
                                            <img id="gora-21" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490251_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490251_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-22" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490252_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490252_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-23" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490254_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490254_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-24" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490262_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490262_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-25" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490263_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490263_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-26" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490268_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490268_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-27" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490269_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490269_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-28" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490271_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490271_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-29" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490272_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490272_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-30" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490273_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490273_98p.jpg">
                                        </div>


                                        <div class="image">
                                            <img id="gora-31" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490274_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490274_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-32" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490275_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490275_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-33" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490280_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/490280_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-34" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690104_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690104_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-35" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690104_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690104_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-36" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690106_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690106_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-37" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690106_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690106_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-38" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_96p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_96p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-39" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-40" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690109_98p.jpg">
                                        </div>


                                        <div class="image">
                                            <img id="gora-41" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690110_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690110_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-42" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690111_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690111_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-43" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690113_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690113_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="gora-44" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690114_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/gora/690114_98p.jpg">
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-right">
                                    <h3 class="header-level-4">Krok 2</h3>
                                    <div class="clotes" id="odziez_gora-wybrane">
                                        <div class="info">Kliknutím na obrázek zvolíte oblečení.</div>
                                        <div id="odziez_gora-slider" class="odziez_slider"></div>
                                    </div>
                                    <div class="buttons">
                                        <!-- <button type="button" class="btn-form" onclick="goTo(1)">Zpět</button> -->
                                        <button type="button" class="btn-form" onclick="goTo(3, 'glowny')">Dále</button>
                                    </div>
                                    <div class="form-error-msg" id="goToStep3msg"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="step step-3">
                            <div class="inner content">
                                <div class="col-left">
                                    <h3 class="header-level-4 header-mobile">Krok 3</h3>
                                    <div class="header">Zvolte dolní část</div>
                                    <div class="images" id="odziez_dol">
                                        <div class="image">
                                            <img id="dol-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390178_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390178_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-3" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390180_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390180_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390181_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390181_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390182_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390182_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-6" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390184_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390184_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-7" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390185_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390185_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-8" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390186_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390186_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-9" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390188_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/390188_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-10" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790114_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790114_98p.jpg">
                                        </div>


                                        <div class="image">
                                            <img id="dol-11" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790115_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790115_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-12" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790116_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790116_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-13" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790118_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790118_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-14" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790119_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790119_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-15" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790120_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790120_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-16" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790121_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790121_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-17" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790123_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790123_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dol-18" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790124_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dol/790124_98p.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <h3 class="header-level-4">Krok 3</h3>
                                    <div class="clotes" id="odziez_dol-wybrane">
                                        <div class="info">Kliknutím na obrázek zvolíte oblečení.</div>
                                        <div id="odziez_dol-slider" class="odziez_slider"></div>
                                    </div>
                                    <div class="buttons">
                                        <button type="button" class="btn-form" onclick="goTo(2, 'glowny')">Zpět</button>
                                        <button type="button" class="btn-form" onclick="goTo(4, 'glowny')">Dále</button>
                                    </div>
                                    <div class="form-error-msg" id="goToStep4msg"></div>
                                </div>
                            </div>
                        </div>
                        <div class="step step-4">
                            <div class="inner content">
                                <div class="col-left">
                                    <h3 class="header-level-4 header-mobile">Krok 4</h3>
                                    <div class="header">Zvolte doplňky</div>
                                    <div class="images" id="dodatki">
                                        <div class="image">
                                            <img id="dodatek-1" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/901083_97f.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/901083_97f.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-2" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/902467_98s.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/902467_98s.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-3" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/902468_98s.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/902468_98s.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-4" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/905121_98s.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/905121_98s.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-5" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/911004_97f.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/911004_97f.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-6" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/911004_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/911004_98p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-7" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/913225_98b.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/913225_98b.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-8" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/915061_98b.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/915061_98b.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-9" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/915062_98b.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/915062_98b.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-10" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/916064_98b.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/916064_98b.jpg">
                                        </div>


                                        <div class="image">
                                            <img id="dodatek-11" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/993039_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/993039_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-12" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/994025_97p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/994025_97p.jpg">
                                        </div>
                                        <div class="image">
                                            <img id="dodatek-13" src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/995047_98p.jpg" alt="" data-src="<?php echo get_template_directory_uri(); ?>/assets/img/konkurs/dodatki/995047_98p.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-right">
                                    <h3 class="header-level-4">Krok 4</h3>
                                    <div class="clotes" id="dodatki_wybrane">
                                        <div class="info">Kliknutím na obrázek, zvolíte doplňky (max. 3)</div>
                                        <div id="odziez_dodatki-slider" class="odziez_slider"></div>
                                    </div>
                                    <div class="buttons">
                                        <button type="button" class="btn-form" onclick="goTo(3, 'glowny')">Zpět</button>
                                        <button type="button" class="btn-form" onclick="goTo(5, 'glowny')"><span>Dále</span></button>
                                    </div>
                                    <div class="form-error-msg" id="goToStep5msg"></div>
                                </div>
                            </div>
                        </div>
                        <input id="items_gora" type="hidden" name="items_gora" />
                        <input id="items_dol" type="hidden" name="items_dol" />
                        <input id="items_dod" type="hidden" name="items_dod" />
                        <div class="step step-5">
                            <div class="content">
                                <h3 class="header-level-4">Krok 5</h3>
                                <div class="header">Potvrďte outfit a odešlete přihlášku</div>
                                <div class="clotes">
                                    <div id="odziez_summary" class="odziez_slider"></div>
                                </div>
                                <div class="buttons">
                                    <button type="button" class="btn-form" onclick="goTo(4, 'glowny')">Zpět</button>
                                    <button id="goToStep6" type="button" class="btn-form btn-form--black" onclick="goTo(6, 'glowny')"><span>Poslat</span></button>
                                </div>
                                <div class="form-error-msg" id="goToStep6msg"></div>
                            </div>
                        </div>
                        <div class="step step-6">
                            <div class="content">
                                <h3 class="header-level-4">Děkujeme!</h3>
                                <div class="header">Na dříve uvedenou e-mailovou adresu jsme zaslali zprávu<br>přejdi do svojí schránky a potvrď účast v soutěži.</div>
                                <div class="header">Pokud jste od nás neobdrželi žádnou zprávu, zkontrolujte složku SPAM.</div>
                                <a href="<?php echo home_url(); ?>" class="btn-form closemagnific">VRÁTIT SE NA HLAVNÍ STRÁNKU</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="contest-popup2" class="zoom-anim-dialog mfp-hide">
    <div class="section contest-section contest-section-2">
        <div class="container">
            <div class="content-layout start-step" id="contest-tygodniowy">
                <div class="desc-col active">
                    <h2 class="header-level-2">TÝDENNÍ SOUTĚŽ</h2>
                    <h2 class="header-level-1">
                        PORAĎTE OSTATNÍM, JAK DOBŘE VYPADAT BĚHEM NÁROČNÉHO DNE V PRÁCI
                    </h2>
                    <p>
                        V několika slovech nám popište, jaký je Vaše „zlatá střední cesta” na úspěch v kanceláři. 
                        Každý týden můžete vyhrát vouchery na nákupy v ORSAY v hodnotě <nobr><b>3000 Kč!</b></nobr>
                    </p>
                </div>
                <div class="form-col">
                    <form class="form">
                       <div class="step step-1 active">
                            <div class="content">
                                <h3 class="header-level-4">Krok 1</h3>
                                <div class="controls">
                                    <label class="control-box">
                                        <span class="label-txt">Jméno a příjmení</span>
                                        <input id="flname2" type="text" class="control-form" name="flname2">
                                        <div id="flname2-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <span class="label-txt">Email</span>
                                        <input id="email2" type="email" class="control-form" name="email2">
                                        <div id="email2-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <div class="checkbox-ctn">
                                            <input id="agree2" type="checkbox" class="control-form" name="agree2">
                                            <span class="state"></span>
                                        </div>
                                        <span class="label-txt label-checkbox">POTVRZUJI, ŽE JSEM SE SEZNÁMIL/A S PRAVIDLY A SOUHLASÍM S JEJICH ZNĚNÍ. PROHLAŠUJI, ŽE JSEM SE SEZNÁMIL/A S POLITIKOU OCHRANY OSOBNÍCH ÚDAJŮ</span>
                                        <div id="agree2-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    <label class="control-box">
                                        <div class="checkbox-ctn">
                                            <input id="newsletter2" type="checkbox" class="control-form" name="newsletter2">
                                            <span class="state"></span>
                                        </div>
                                        <span class="label-txt label-checkbox">Chci se přihlásit k odběru novinek, a proto souhlasím s tím, že budu zasílat marketingový obsah (obchodní informace) na svou e-mailovou adresu o nových produktech, spolupráci a událostech ve značce Orsay.</span>
                                        <div id="newsletter2-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                    
                                    <a href="<?php echo get_template_directory_uri(); ?>/assets/soutez_orsay.pdf" class="get-regulations" download>Pravidla a podmínky</a>
                                </div>
                                <button type="button" id="goToStep2-tygodniowy" class="btn-form" onclick="goTo(2, 'tygodniowy')"><span>Dále</span></button>
                                <div class="form-error-msg" id="goToStep2msg_tygodniowy"></div>
                                <div class="form-foot">
                                    Záleží nám na Tvém soukromí. Správcem osobních údajů poskytnutých ve formuláři bude společnost ORSAY GmbH, Sitz der Gesell: Willstaett, Německo, zapsána v soudním rejstříku Amtsegericht Freiburg HRB 370336, zastoupena v Polsku přes Ordipol Sp. z o. o. , ul. Logistyczna 1, 50-040 Bielany Wrocławskie.
                                    Poskytnuté údaje budou zpracovány pro účely uvedené v pravidlech soutěže, prosíme Tě tedy o jejich přečtení. Podrobnosti týkající se zpracování osobních údajů
                                </div>
                            </div>
                        </div>
                         <div class="step step-2">
                            <div class="content">
                            <div class="content">
                                <h3 class="header-level-4">Krok 2</h3>
                                <div class="controls">
                                    <label class="control-box" style="margin-bottom:25px;">
                                        <span class="label-txt">POPIŠTE, JAKÁ JE VAŠE MÓDNÍ "ZLATÁ STŘEDNÍ CESTA" NA ÚSPĚCH V PRÁCI:</span>
                                        <textarea id="description" name="description" type="text" class="control-form" rows="6"></textarea>
                                        <div id="description-error" class="error-msg">Toto pole je povinné</div>
                                    </label>
                                </div>
                                <button type="button" class="btn-form" onclick="goTo(3, 'tygodniowy')"><span>Odeslat</span></button>
                                <div class="form-error-msg" id="goToStep3msg_tygodniowy"></div>
                            </div>
                            </div>
                        </div>
                        <div class="step step-3">
                            <div class="content">
                                <h3 class="header-level-4">Děkujeme!</h3>
                                <div class="header">Na dříve uvedenou e-mailovou adresu jsme zaslali zprávu<br>přejdi do svojí schránky a potvrď účast v soutěži.</div>
                                <a href="<?php echo home_url(); ?>" class="btn-form closemagnific">VRÁTIT SE NA HLAVNÍ STRÁNKU</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mobile-menu" id="mobile-menu">
    <div id="mobile-menu-close"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt="close"></div>
    <ul class="menu">
        <li><a href="#section1">START</a></li>
        <li><a href="#section2">O AKCI</a></li>
        <li><a href="#section3">JITKA NOVÁČKOVÁ</a></li>
        <li><a href="#section4">KAMPAŇOVÉ VIDEO</a></li>
        <li><a href="#section6">MASTERCARD®</a></li>
        <li><a href="#section7">SOUTĚŽIT</a></li>
        <!--<li><a href="#section5">TÝDENNÍ SOUTĚŽ</a></li>-->
        <li><a href="#section8">VÍTĚZKY</a></li>
        <!--<li><a href="#section8">VÝSLEDKY</a></li>-->
    </ul>
</div>
<header id="page-header">
    <div class="container">
        <div class="logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/orsay_logo.svg" alt="">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/orsay_logo_black.svg" alt="">
        </div>
        <div class="logo-mastercard">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mastercard_priceless.png" alt="">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mastercard_priceless_black.png" alt="">
        </div>
        <div id="mobile-menu-btn"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/menu.svg" alt=""></div>
    </div>
</header>
<div id="fullpage">
    <section class="section first-section" id="section1">
    <div class="soundtoggle"></div>
    <video id="startVideo" loop muted playsinline>
        <source id="video" src="<?php echo get_template_directory_uri(); ?>/assets/video/video_start.mp4" type="video/mp4">
    </video>
        <div class="container">
            <div class="inner">
                <h1 class="header-level-1">
                    INSPIRUJTE SE OUTFITY JITKY NOVÁČKOVÉ<br>A UKAŽTE SVŮJ JEDINEČNÝ STYL
                </h1>
                <div class="buttons">
                    <a href="#section3" class="btn btn-action" onclick="goPage(event, 3)">UKÁZAT JITČIN VÝBĚR</a>
                    <a href="#section7" class="btn btn-action" onclick="goPage(event, 7)">SOUTĚŽIT</a>
                </div>
            </div>
        </div>
    </section>
    <section class="section second-section" id="section2">
        <div class="inner">
            <div class="bg-row">
                <div class="bg-col-1"></div>
                <div class="bg-col-2"></div>
            </div>
            <div class="container">
                <div class="side">
                    <div class="content">
                        <h2 class="header-level-1">
                            BUSINESS KOLEKCE DOPORUČENÁ JITKOU NOVÁČKOVOU
                        </h2>
                        <p class="p-level-1">
                            První dojem je nejdůležitější, tak proč na to jít nejít stylově?
                        </p>
                        <p>
                            Jitka Nováčková, modelka a youtuberka, doporučuje sáhnout po klasických formálních střizích a propojit je s moderními trendy!
                        </p>
                        <ul class="list">
                            <li>
                                <div class="first">
                                    <div class="img-box"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/dress.png" alt=""></div>
                                    <span class="txt">Inspirujte se Jitkou Nováčkovou a zvolte si kousek z business kolekce</span>
                                </div>
                            </li>
                            <li>
                                <div class="first">
                                    <div class="img-box"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/gift.png" alt=""></div>
                                    <span class="txt">Vyzvedněte si v kamenné prodejně dárek za nákup z business kolekce*</span>
                                </div>
                            </li>
                            <li>
                                <div class="first">
                                    <div class="img-box"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/discount_black.svg" alt=""></div>
                                    <span class="txt">Získejte 10% zpět za Vaše nákupy</span>
                                </div>
                                <div class="second">
                                    <a href="#section6" onclick="goPage(event, 5)" class="btn btn--small btn-action btn--pn">PODROBNOSTI</a>
                                </div>
                            </li>
                            <li>
                                <div class="first">
                                    <div class="img-box"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/eshop-big.png" alt=""></div>
                                    <span class="txt">Účastněte se soutěže a získejte voucher v hodnotě 25 000 CZK!</span>
                                </div>
                                <div class="second buttons">
                                    <a href="#section7" onclick="goPage(event, 6)" class="btn btn--pn  btn-action btn--pn2">SOUTĚŽIT</a>
                                </div>
                            </li>
                        </ul>
                        
                    </div>
                </div>
                <div class="main"></div>
            </div>
        </div>
    </section>
    <section class="section seven-section" id="section3">
        <div class="container">
            <div class="catalog-slider-container">
                <div class="catalog-slider-all">
                    <a href="https://www.orsay.com/cs-cz/trendy/business/?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item item-one slider-photo">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/lookbook_photo1.png" alt="">
                            <div class="img-label">
                                <div class="note">Jitka's Choice</div>
                                <!--<ul class="clothes">
                                    <li>ŠATY 799</li>
                                    <li>PENĚŽENKA* 349 </li>
                                </ul>-->
                            </div>
                        </div>
                    </a>
                    <a href="https://www.orsay.com/cs-cz/trendy/business/?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item item-two slider-photo">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/lookbook_photo2.png" alt="">
                            <div class="img-label">
                                <div class="note">Jitka's Choice</div>
                                <!--<ul class="clothes">
                                    <li>ŠATY 799</li>
                                    <li>PENĚŽENKA* 349 </li>
                                </ul>-->
                            </div>
                        </div>
                    </a>
                    <a href="https://www.orsay.com/cs-cz/trendy/business/?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item item-three slider-photo">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/lookbook_photo3.png" alt="">
                            <div class="img-label">
                                <div class="note">Jitka's Choice</div>
                                <!--<ul class="clothes">
                                    <li>ŠATY 799</li>
                                    <li>PENĚŽENKA* 349 </li>
                                </ul>-->
                            </div>
                        </div>
                    </a>
                    <a href="https://www.orsay.com/cs-cz/trendy/business/?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item item-three slider-photo">
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/lookbook_photo4.png" alt="">
                            <div class="img-label">
                                <div class="note">Jitka's Choice</div>
                                <!--<ul class="clothes">
                                    <li>ŠATY 799</li>
                                    <li>PENĚŽENKA* 349 </li>
                                </ul>-->
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="section fifth-section" id="section4">
        <div class="container">
            <div class="inner">
                <div class="side">
                    <h2 class="header-level-3">TIPY NA OUTFITY Z VIDEA</h2>
                    <div class="product-slider">
                        <div class="row-slider">
                            <a href="https://www.orsay.com/cs-cz/vzorovane-kosilove-saty-490269016000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" target="_blank" class="item backstage_photo">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/490269_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">ŠATY</div>
                                    <div class="price">799 Kč</div>
                                </div>
                            </a>
                            <a href="https://www.orsay.com/cs-cz/sako-s-3-4-rukavy-480209526000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_cam"  target="_blank" class="item backstage_photo">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/480209_98p.jpg" alt="">  
                                </div>
                                <div class="image-details">
                                    <div class="name">SAKO</div>
                                    <div class="price">1.299 Kč</div>
                                </div>
                            </a>
                        </div>
                        <div class="row-slider">
                            <a href="https://www.orsay.com/cs-cz/kalhoty-s-opaskem-390184526000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_cam" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/390184_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">KALHOTY</div>
                                    <div class="price">799 Kč</div>
                                </div>
                            </a>
                            <a href="https://www.orsay.com/cs-cz/volna-halenka-se-zahyby-690113016000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&u" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/690113_97p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">HALENKA</div>
                                    <div class="price">549 Kč</div>
                                </div>
                            </a>
                        </div>
                        <div class="row-slider">
                            <a href="https://www.orsay.com/cs-cz/maxi-kabelka-902468660000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/902468_98s.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">KABELKA</div>
                                    <div class="price">949 Kč</div>
                                </div>
                            </a>
                            <a href="https://www.orsay.com/cs-cz/penezenka-s-krokodylim-vzorem-911004016000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/911004_97f.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">PENĚŽENKA</div>
                                    <div class="price">349 Kč</div>
                                </div>
                            </a>
                        </div>
                        <div class="row-slider">
                            <a href="https://www.orsay.com/cs-cz/kalhoty-s-puky-390186235000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/390186_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">KALHOTY</div>
                                    <div class="price">799 Kč</div>
                                </div>
                            </a>
                            <a href="https://www.orsay.com/cs-cz/prilehave-sako-480211235000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/480211_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">SAKO</div>
                                    <div class="price">1.299 Kč</div>
                                </div>
                            </a>
                        </div>
                        <div class="row-slider">
                            <a href="https://www.orsay.com/cs-cz/saty-s-ackovou-sukni-490273235000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/490273_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">ŠATY</div>
                                    <div class="price">949 Kč</div>
                                </div>
                            </a>
                            <a href="https://www.orsay.com/cs-cz/halenka-se-zvirecim-vzorem-690114235000.html?nsctrid=70419340000&utm_source=landingpage&utm_medium=landingpage&utm_campaign=jitka&utm_content=product&utm_term=jitkalp" class="item backstage_photo" target="_blank">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/packshots/690114_98p.jpg" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">Halenka</div>
                                    <div class="price">649 Kč</div>
                                    <!--<div class="name">BRZY DOSTUPNÉ</div>-->
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="main mobile">
                    <div class="video-box">
                        <video id="video_backstage" style="max-width: 100%; max-height: 100%" poster="<?php echo get_template_directory_uri(); ?>/assets/img/okladka_orsay.png">
                            <source src="<?php echo get_template_directory_uri(); ?>/assets/video/backstage.mp4" type="video/mp4">
                        </video>
                        <div class="video-play" id="video-play">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/Play_Button.svg" alt="">
                            <div class="txt">KAMPAŇOVÉ VIDEO</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section fourth-section" id="section6">
        <div class="inner">
            <div class="container">
                <div class="section-col section-col-1 vcenter">
                    <h1 class="header-level-1">
                        ZÍSKEJTE ZPĚT<br>
                        Z VAŠICH NÁKUPŮ V ORSAY
                    </h1>
                </div>
                <div class="section-col section-col-2">
                    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo_mastercard.png" alt="">
                    <p>
                    <b>Získejte s Priceless Specials 10 % zpět ze svých nákupů v ORSAY</b>
                    </p>
                    <p>
                    Rozlučte se s létem ve velkém stylu. ORSAY přináší novou business kolekci oblečení a vy máte <b>od 22. 8. do 15. 9.</b>
                    jedinečnou příležitost získat z každého nákupu 10 % zpět. <b>Stačí platit kartou Mastercard® a zapojit se do programu Priceless Specials.</b>
                    </p>
                    <p>
                        <b>Co je Priceless Specials?</b>
                    </p>
                    <a id="mastercard_specials" href="http://www.pricelessspecials.cz/nabidka-orsay_cz-39.html" target="_blank" class="btn">Zaregistrujte se v programu</a>
                    <!-- <ul class="list">
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/icon-card.svg" alt="">
                            </div>
                            <div class="txt">Zaregistruje kartu MASTERCARD® v programu Priceless Special!</div>
                        </li>
                        <li>
                            <div class="img-box">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/discount_56.svg" alt="">
                            </div>
                            <div class="txt">Účastněte se akce Cashback a získejte 10% zpět z nákupů v prodejnách ORSAY</div>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
    </section>
    
    <!--<section class="section fifth-section fifth-section-two" id="section5">
        <div class="container">
            <div class="inner">
                    <div class="main">
                        <div class="video-box">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/photo_video.png" alt="">
                            <div class="video-play">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/Play_Button.svg" alt="">
                                <div class="txt">KAMPAŇOVÉ VIDEO</div>
                            </div>
                        </div>
                    </div>
                <div class="side">
                    <h2 class="header-level-3">TIPY NA OUTFITY Z VIDEA</h2>
                    <div class="product-slider">
                        <div class="row-slider">
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_1-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">HALENKA</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_2-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">KALHOTY</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                        </div>
                        <div class="row-slider">
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_3-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">HALENKA</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_4-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">HALENKA</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                        </div>
                        <div class="row-slider">
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_1-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">HALENKA</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="img-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/clothes_2-2.png" alt="">
                                </div>
                                <div class="image-details">
                                    <div class="name">KALHOTY</div>
                                    <div class="price">49 zł</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>-->
    
    <section class="section third-section" id="section7">
        <div class="inner">
            <div class="container">
                <div class="side">
                    <div class="content">
                        <h2 class="header-level-2">TÝDENNÍ SOUTĚŽ</h2>
                        <h3 class="header-level-1">PORAĎTE OSTATNÍM, JAK DOBŘE VYPADAT BĚHEM NÁROČNÉHO DNE V PRÁCI</h3>
                        <p>
                            V několika slovech nám popište, jaký je Vaše „zlatá střední cesta“ na úspěch v kanceláři. 
                            Každý týden můžete vyhrát vouchery na nákupy v ORSAY v hodnotě <nobr><b>3000 Kč!</b></nobr>
                        </p>
                        <p>Děkujeme za účast! Výsledky se objeví brzy.</p>
                    </div>
                </div>
                <div class="main">
                        <h2 class="header-border">HLAVNÍ SOUTĚŽ</h2>
                        <h2 class="header-level-1">
                            UKAŽTE NÁM,<br>V ČEM DOBUDETE SVĚT BUSINESSU
                        </h2>
                        <p>
                            Vytvořte si v našem generátoru outfitů<br>Váš vysněný business look a vyhrajte voucher<br> v hodnotě <b>25 000 Kč</b> na nákupy v ORSAY!
                        </p>
                        <p>Děkujeme za účast! Výsledky se objeví brzy.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="section results-section" id="section8">
        <div class="container">
            <div class="inner">
                <div class="text">
                    VÝSLEDKY TÝDENNÍCH SOUTĚŽÍCH SE OBJEVÍ ZDE DO 2.10.2019. 
                </div>
                <div class="text">
                    VÍTĚZKA HLAVNÍ SOUTĚŽE BUDE VYHLÁŠENA DO 2.10.2019.
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="section winners" id="section9">
        <div class="inner">
            <div class="container">
                <div class="col-winners-1">
                    <h2 class="header-level-1">ZWYCIĘŻCZYNIE</h2>
                    <h3 class="header-level-2">KONKURS TYGODNIOWY</h3>
                    <div class="persons">
                        <div class="item">
                            <div class="name"><b>Monika Wójcik</b></div>
                            <div class="desc">
                                Najważniejsze jest pierwsze wrażenie, 
                                a to najlepiej jest zrobić w stylowy sposób! Jitka Novaczkova, modelka 
                                i youtuberka proponuje do tego klasyczne, formalne kroje połączone z gorącymi trendami! 
                            </div>
                        </div>
                        <div class="item">
                            <div class="name"><b>Katarzyna Nowak</b></div>
                            <div class="desc">
                                Najważniejsze jest pierwsze wrażenie, 
                                a to najlepiej jest zrobić w stylowy sposób! Jitka Novaczkova, modelka 
                                i youtuberka proponuje do tego klasyczne, formalne kroje połączone z gorącymi trendami! 
                            </div>
                        </div>
                        <div class="item">
                            <div class="name"><b>Agata Frankowska</b></div>
                            <div class="desc">
                                Najważniejsze jest pierwsze wrażenie, 
                                a to najlepiej jest zrobić w stylowy sposób! Jitka Novaczkova, modelka 
                                i youtuberka proponuje do tego klasyczne, formalne kroje połączone z gorącymi trendami! 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-winners-2">
                    <h2 class="header-border">Konkurs główny</h2>
                    <div class="person-main">
                        <div class="name"><b>Magda Adamska</b></div>
                        <div class="desc">
                            Najważniejsze jest pierwsze wrażenie, 
                            a to najlepiej jest zrobić w stylowy sposób! Jitka Novaczkova, modelka 
                            i youtuberka proponuje do tego klasyczne, formalne kroje połączone z gorącymi trendami! 
                        </div>
                        <div class="products">
                            <div class="item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/prod-winner-1.png" alt="">
                            </div>
                            <div class="item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/prod-winner-2.png" alt="">
                            </div>
                            <div class="item">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/prod-winner-3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
</div>

<?php
endwhile;
endif;

get_footer();
?>
