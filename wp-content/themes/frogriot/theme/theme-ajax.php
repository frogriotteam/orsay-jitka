<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! class_exists( 'Theme_Ajax' ) ) :

class Theme_Ajax{

    function __construct(){

        //ajax 1
        add_action( 'wp_ajax_process_step_one', array($this, 'process_step_one') );
        add_action( 'wp_ajax_nopriv_process_step_one', array($this, 'process_step_one') );
        add_action( 'wp_ajax_process_step_final', array($this, 'process_step_final') );
        add_action( 'wp_ajax_nopriv_process_step_final', array($this, 'process_step_final') );

        //ajax 2
        add_action( 'wp_ajax_process_weekly', array($this, 'process_weekly') );
        add_action( 'wp_ajax_nopriv_process_weekly', array($this, 'process_weekly') );
        add_action( 'wp_ajax_process_weekly_final', array($this, 'process_weekly_final') );
        add_action( 'wp_ajax_nopriv_process_weekly_final', array($this, 'process_weekly_final') );

    }

    function process_step_one() {

        $passed = false;
        $errors = false;
        $emailerror = '';
        $flnameerror = '';
        $agreeerror = '';

        //security check - start
        check_ajax_referer( 'fr_nonce', 'security' );

        $email = $_POST['email'];
        $flname = $_POST['flname'];
        $agree = $_POST['agree'];
        $newsletter = $_POST['newsletter'];

        if( $agree != 1 ){
            $errors = true;
            $agreeerror = 'Toto pole je povinné';
        }

        if( $newsletter != 1 ){
            $newsletter_checked = 0;
        }else{
            $newsletter_checked = 1;
        }

        if ( ! $flname ) {
            $errors = true;
            $flnameerror = 'Toto pole je povinné';
        }else{
            //sanitize fname
            $flname = sanitize_text_field($flname);
        }

        //email validation
        if ( ! $email ) {
            $errors = true;
            $emailerror = 'Toto pole je povinné';
        }else if ( ! is_email( $email ) || strlen($first) > 64 ) {
            $errors = true;
            $emailerror = 'Neplatná e-mailová adresa';
        }else{
            //sanitize email
            $email = sanitize_email($email);
        }

        if( $errors == false ){

            $date = date('Y-m-d H:i:s');

            global $wpdb;
            $table = $wpdb->prefix."zgloszenia";
            $save_form = $wpdb->insert( 
                $table, 
                array(
                    'date'  => $date,
                    'fname' => $flname,
                    'email' => $email,
                    'newsletter' => $newsletter_checked
                )
            );

            //if saved
            if( ! isset( $save_form['error'] ) ){

                $title = "Soutěž ORSAY: potvrzení přihlášky";
    
                $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <title>Orsay</title>
                    <style>
                        @import url(\'https://fonts.googleapis.com/css?family=Oswald&display=swap&subset=latin-ext\');
                        @import url(\'https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap&subset=latin-ext\');
                    </style>
                    <style type="text/css">
                        #outlook a {padding:0;}
                        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
                        .ExternalClass {width:100%;}
                        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
                        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
                        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
                        a img {border:none;}
                        .image_fix {display:block; max-width: 100%}
                        p {margin: 30px 0; line-height: 1.6}
                        h1, h2, h3, h4, h5, h6 {color: black !important;}
                        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
                        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
                            color: red !important;
                         }
                
                        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
                            color: purple !important;
                        }
                        table td {border-collapse: collapse;}
                        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
                        a {color: orange;}
                        @media only screen and (max-device-width: 480px) {
                            a[href^="tel"], a[href^="sms"] {
                                        text-decoration: none;
                                        color: black; /* or whatever your want */
                                        pointer-events: none;
                                        cursor: default;
                                    }
                
                            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                                        text-decoration: default;
                                        color: orange !important; /* or whatever your want */
                                        pointer-events: auto;
                                        cursor: default;
                                    }
                        }
                
                        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
                            a[href^="tel"], a[href^="sms"] {
                                        text-decoration: none;
                                        color: blue;
                                        pointer-events: none;
                                        cursor: default;
                                    }
                
                            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                                        text-decoration: default;
                                        color: orange !important;
                                        pointer-events: auto;
                                        cursor: default;
                                    }
                        }
                
                        @media only screen and (max-device-width: 800px) {
                            .mobile-offset {
                                margin-top: 0 !important;
                                border: none !important;
                            }
                        }
                
                        @media only screen and (-webkit-min-device-pixel-ratio: 2) {
                            /* Put your iPhone 4g styles in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:.75){
                            /* Put CSS for low density (ldpi) Android layouts in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:1){
                            /* Put CSS for medium density (mdpi) Android layouts in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:1.5){
                            /* Put CSS for high density (hdpi) Android layouts in here */
                        }
                    </style>
                    <!--[if IEMobile 7]>
                    <style type="text/css">
                        /* Targeting Windows Mobile */
                    </style>
                    <![endif]-->
                    <!--[if gte mso 9]>
                    <style>
                        /* Target Outlook 2007 and 2010 */
                    </style>
                    <![endif]-->
                </head>
                <body>
                    <div style="width: 100%; max-width: 800px; margin: 0 auto;">
                    <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" width="100%">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="19"></td>
                                    <td height="19"></td>
                                </tr>
                                <tr>
                                    <td width="132" valign="center" height="63" style="border-right: 1px solid #000">
                                        <img src="'.get_template_directory_uri().'/mailings/mailing1/orsay_logo_black.png" alt="">
                                    </td>
                                    <td width="132" valign="center" height="63" style="padding-left: 16px">
                                        <img src="'.get_template_directory_uri().'/mailings/mailing1/logo_mastercard.png" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="19"></td>
                                    <td height="19"></td>
                                </tr>
                            </table>
                            <img class="image_fix" src="'.get_template_directory_uri().'/mailings/mailing1/image.png" alt="Your alt text" title="Your title text" />
                            <table cellpadding="0" cellspacing="0" border="0" align="center" class="mobile-offset" style="margin-top: -123px; border: 1px solid #000; position: relative">
                                <tr>
                                    <td style="max-width: 600px; padding: 27px; background: #fff; position: relative;" align="center">
                                        <p style="text-align: center; font-size: 30px; font-family: \'Oswald\', sans-serif;">DĚKUJEME VÁM ZA REGISTRACI </p>
                                        <p style="text-align: center; font-size: 14px; font-family: \'Roboto Slab\', serif;">
                                            První krok už máte za sebou! Teď už zbývá odpovědět na soutěžní otázku a poslat přihlášku!
                                        </p>
                                        <p style="text-align: center; font-size: 14px; font-family: \'Roboto Slab\', serif;">
                                            Připomínáme, že každý týden můžete vyhrát poukázku v hodnotě 3000 Kč! 
                                        </p>
                                        <a href="'.home_url().'/confirmation?email='.$email.'&contest=1" class="btn" style="display: inline-block; padding: 14px 40px; background: #D4B45B; color: #fff; text-transform: uppercase; font-family: \'Oswald\', sans-serif; text-decoration: none">Potvrďte účast</a>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="77"></td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                <tr>
                                    <td height="28" style="padding-left: 20px">
                                        <a href="#"><img src="'.get_template_directory_uri().'/mailings/mailing1/orsay_logo_black.png" width="97" alt=""></a>
                                    </td>
                                    <td height="28" width="48"></td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.facebook.com/orsaypl/" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/facebook.png" height="20" alt=""></a>
                                    </td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.instagram.com/orsay/?hl=pl" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/instagram.png" height="20" alt=""></a>
                                    </td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.orsay.com/pl-pl/" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/shopping_bag.png" height="20" alt=""></a>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="38"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                    </div>
                </body>
                </html>
                ';

                // Notification
                $sendmail = wp_mail( $email, $title, $message);

                if( $sendmail ){ 
                    $passed = true;
                }

            }

        }

        //response
        $response = array(
            'passed' => $passed,
            'errors' => $errors,
            'errormsgs' => array(
                'email' => $emailerror,
                'flname' => $flnameerror,
                'agree' => $agreeerror
            )
        );

        //send response
        wp_send_json($response);
        die();
    }

    function process_step_final() {

        $passed = false;
        $errors = false;

        //security check - start
        check_ajax_referer( 'fr_nonce', 'security' );

        $items_gora = $_POST['items_gora'];
        $items_dol = $_POST['items_dol'];
        $items_dod = $_POST['items_dod'];
        $email = $_POST['email'];

            global $wpdb;
            $table = $wpdb->prefix."zgloszenia";

            $update_form = $wpdb->update( 
                $table, 
                array(
                    'items_gora' => $items_gora,
                    'items_dol' => $items_dol,
                    'items_dod' => $items_dod
                ),
                array(
                    'email' => $email
                )
            );

            //if saved
            if( ! isset( $update_form['error'] ) ){

                $passed = true;

            }

        

        //response
        $response = array(
            'passed' => $passed,
        );

        //send response
        wp_send_json($response);
        die();
    }

    function process_weekly() {

        $passed = false;
        $errors = false;
        $emailerror = '';
        $flnameerror = '';
        $agreeerror = '';

        //security check - start
        check_ajax_referer( 'fr_nonce', 'security' );

        $email = $_POST['email'];
        $flname = $_POST['flname'];
        $agree = $_POST['agree'];
        $newsletter = $_POST['newsletter'];

        if( $agree != 1 ){
            $errors = true;
            $agreeerror = 'Toto pole je povinné';
        }

        if( $newsletter != 1 ){
            $newsletter_checked = 0;
        }else{
            $newsletter_checked = 1;
        }

        if ( ! $flname ) {
            $errors = true;
            $flnameerror = 'Toto pole je povinné';
        }else{
            //sanitize fname
            $flname = sanitize_text_field($flname);
        }

        //email validation
        if ( ! $email ) {
            $errors = true;
            $emailerror = 'Toto pole je povinné';
        }else if ( ! is_email( $email ) || strlen($first) > 64 ) {
            $errors = true;
            $emailerror = 'Neplatná e-mailová adresa';
        }else{
            //sanitize email
            $email = sanitize_email($email);
        }

        if( $errors == false ){

            $date = date('Y-m-d H:i:s');

            global $wpdb;
            $table = $wpdb->prefix."zgloszenia_tygodniowy";
            $save_form = $wpdb->insert( 
                $table, 
                array(
                    'date'  => $date,
                    'fname' => $flname,
                    'email' => $email,
                    'newsletter' => $newsletter_checked
                )
            );

            //if saved
            if( ! isset( $save_form['error'] ) ){

                $title = "Soutěž ORSAY: potvrzení přihlášky";
    
                $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    <title>Orsay</title>
                    <style>
                        @import url(\'https://fonts.googleapis.com/css?family=Oswald&display=swap&subset=latin-ext\');
                        @import url(\'https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap&subset=latin-ext\');
                    </style>
                    <style type="text/css">
                        #outlook a {padding:0;}
                        body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
                        .ExternalClass {width:100%;}
                        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
                        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
                        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
                        a img {border:none;}
                        .image_fix {display:block; max-width: 100%}
                        p {margin: 30px 0; line-height: 1.6}
                        h1, h2, h3, h4, h5, h6 {color: black !important;}
                        h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
                        h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
                            color: red !important;
                         }
                
                        h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
                            color: purple !important;
                        }
                        table td {border-collapse: collapse;}
                        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
                        a {color: orange;}
                        @media only screen and (max-device-width: 480px) {
                            a[href^="tel"], a[href^="sms"] {
                                        text-decoration: none;
                                        color: black; /* or whatever your want */
                                        pointer-events: none;
                                        cursor: default;
                                    }
                
                            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                                        text-decoration: default;
                                        color: orange !important; /* or whatever your want */
                                        pointer-events: auto;
                                        cursor: default;
                                    }
                        }
                
                        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
                            a[href^="tel"], a[href^="sms"] {
                                        text-decoration: none;
                                        color: blue;
                                        pointer-events: none;
                                        cursor: default;
                                    }
                
                            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                                        text-decoration: default;
                                        color: orange !important;
                                        pointer-events: auto;
                                        cursor: default;
                                    }
                        }
                
                        @media only screen and (max-device-width: 800px) {
                            .mobile-offset {
                                margin-top: 0 !important;
                                border: none !important;
                            }
                        }
                
                        @media only screen and (-webkit-min-device-pixel-ratio: 2) {
                            /* Put your iPhone 4g styles in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:.75){
                            /* Put CSS for low density (ldpi) Android layouts in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:1){
                            /* Put CSS for medium density (mdpi) Android layouts in here */
                        }
                        @media only screen and (-webkit-device-pixel-ratio:1.5){
                            /* Put CSS for high density (hdpi) Android layouts in here */
                        }
                    </style>
                    <!--[if IEMobile 7]>
                    <style type="text/css">
                        /* Targeting Windows Mobile */
                    </style>
                    <![endif]-->
                    <!--[if gte mso 9]>
                    <style>
                        /* Target Outlook 2007 and 2010 */
                    </style>
                    <![endif]-->
                </head>
                <body>
                    <div style="width: 100%; max-width: 800px; margin: 0 auto;">
                    <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" width="100%">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="19"></td>
                                    <td height="19"></td>
                                </tr>
                                <tr>
                                    <td width="132" valign="center" height="63" style="border-right: 1px solid #000">
                                        <img src="'.get_template_directory_uri().'/mailings/mailing1/orsay_logo_black.png" alt="">
                                    </td>
                                    <td width="132" valign="center" height="63" style="padding-left: 16px">
                                        <img src="'.get_template_directory_uri().'/mailings/mailing1/logo_mastercard.png" alt="">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="19"></td>
                                    <td height="19"></td>
                                </tr>
                            </table>
                            <img class="image_fix" src="'.get_template_directory_uri().'/mailings/mailing1/image.png" alt="Your alt text" title="Your title text" />
                            <table cellpadding="0" cellspacing="0" border="0" align="center" class="mobile-offset" style="margin-top: -123px; border: 1px solid #000; position: relative">
                                <tr>
                                    <td style="max-width: 600px; padding: 27px; background: #fff; position: relative;" align="center">
                                        <p style="text-align: center; font-size: 30px; font-family: \'Oswald\', sans-serif;">DĚKUJEME VÁM ZA REGISTRACI </p>
                                        <p style="text-align: center; font-size: 14px; font-family: \'Roboto Slab\', serif;">
                                            První krok už máte za sebou! Teď už zbývá odpovědět na soutěžní otázku a poslat přihlášku!
                                        </p>
                                        <p style="text-align: center; font-size: 14px; font-family: \'Roboto Slab\', serif;">
                                            Připomínáme, že každý týden můžete vyhrát poukázku v hodnotě 3000 Kč! 
                                        </p>
                                        <a href="'.home_url().'/confirmation?email='.$email.'&contest=2" class="btn" style="display: inline-block; padding: 14px 40px; background: #D4B45B; color: #fff; text-transform: uppercase; font-family: \'Oswald\', sans-serif; text-decoration: none">PŘEJÍT NA STRÁNKU</a>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="77"></td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%">
                                <tr>
                                    <td height="28" style="padding-left: 20px">
                                        <a href="#"><img src="'.get_template_directory_uri().'/mailings/mailing1/orsay_logo_black.png" width="97" alt=""></a>
                                    </td>
                                    <td height="28" width="48"></td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.facebook.com/orsaypl/" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/facebook.png" height="20" alt=""></a>
                                    </td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.instagram.com/orsay/?hl=pl" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/instagram.png" height="20" alt=""></a>
                                    </td>
                                    <td height="28" width="48" style="text-align: center">
                                        <a href="https://www.orsay.com/pl-pl/" target="_blank"><img src="'.get_template_directory_uri().'/mailings/mailing1/shopping_bag.png" height="20" alt=""></a>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td height="38"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                    </div>
                </body>
                </html>
                ';

                // Notification
                $sendmail = wp_mail( $email, $title, $message);

                if( $sendmail ){ 
                    $passed = true;
                }

            }

        }

        //response
        $response = array(
            'passed' => $passed,
            'errors' => $errors,
            'errormsgs' => array(
                'email' => $emailerror,
                'flname' => $flnameerror,
                'agree' => $agreeerror
            )
        );

        //send response
        wp_send_json($response);
        die();
    }

    function process_weekly_final() {

        $passed = false;
        $errors = false;
        $descriptionerror = '';

        //security check - start
        check_ajax_referer( 'fr_nonce', 'security' );

        $description = $_POST['description'];
        $email = $_POST['email'];

        if ( ! $description || $description == '' ) {
            $errors = true;
            $descriptionerror = 'Toto pole je povinné';
        }elseif( strlen($description) > 1000 ){
            $errors = true;
            $descriptionerror = 'Byl překročen limit počtu znaků.';
        }else{
            $description = sanitize_text_field($description);
        }

        if( $errors == false ){

            global $wpdb;
            $table = $wpdb->prefix."zgloszenia_tygodniowy";

            $update_form = $wpdb->update( 
                $table, 
                array(
                    'description' => $description,
                ),
                array(
                    'email' => $email
                )
            );

            if( ! isset( $update_form['error'] ) ){
                $passed = true;
            }

        }
        

        //response
        $response = array(
            'passed' => $passed,
            'errors' => $errors,
            'errormsgs' => array(
                'description' => $descriptionerror,
            )
        );

        //send response
        wp_send_json($response);
        die();
    }


}

endif;
?>