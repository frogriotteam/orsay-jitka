<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! class_exists( 'Theme_Enqueue' ) ) :

	class Theme_Enqueue {

		function __construct() {

			add_action( 'wp_enqueue_scripts', array($this, 'theme') );
			add_action( 'admin_enqueue_scripts', array($this, 'theme_admin_scripts') );

		}

		function theme() {

			$v = 1;

			if (!is_admin()) {
				wp_deregister_script('jquery');
			}

			//app.css
			wp_register_style( 'app-css', get_template_directory_uri() . '/dist/app.min.css', false, $v );
			wp_enqueue_style( 'app-css' );

			//dist/bundle.min.js
			wp_enqueue_script( 'app-js', get_template_directory_uri() . '/dist/app.min.js', array(), $v, true );
			wp_localize_script( 'app-js', 'FR', array(
				'theme_url' => get_template_directory_uri(),
				'home_url' => esc_url( home_url( '/' ) ),
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'ajax_nonce' => wp_create_nonce('fr_nonce'),
			) );

		}

		function theme_admin_scripts(){

			wp_register_style( 'admin', get_template_directory_uri() . '/assets/css/admin-style.css', false, '1' );
			wp_enqueue_style( 'admin' );

		}

	}

endif;