<?php
if( function_exists( 'acf_add_options_page' ) ){
	acf_add_options_page(array(
		'page_title' 	=> __('Theme options'),
		'menu_title' 	=> __('Theme options'),
		'menu_slug' 	=> 'theme-options',
		'capability' 	=> 'edit_posts',
		'parent_slug'   => '',
		'position' 		=> false,
		'icon_url'		=> false,
	));
}
?>
