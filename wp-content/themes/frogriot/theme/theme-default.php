<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'Theme_Default' ) ) :

	class Theme_Default {

		function __construct() {

			$this->remove_junk();
			add_action('init', array($this, 'deregister_wpembed'));

			/*--------------------------------------------------------------------------------------------
			### ADD THEME IMAGES SIZE ###
			--------------------------------------------------------------------------------------------*/
			
			add_filter( 'remove_orphans', array($this, 'remove_orphans') );

			add_filter( 'wp_mail_content_type', array($this, 'html_mails') );

			$this->menus();


			/* add_image_size( 'project', 635, 403, array( 'center', 'center' ) ); */

		}

		/*--------------------------------------------------------------------------------------------
		### TINYMCE NEW LINE - ENTER INSTEAD OF ENTER SHIFT ###
		--------------------------------------------------------------------------------------------*/
		function my_switch_tinymce_p_br( $settings ) {
			$settings['forced_root_block'] = false;
			return $settings;
		}

		/*--------------------------------------------------------------------------------------------
		### HTML EMAIL ###
		--------------------------------------------------------------------------------------------*/
		function html_mails(){
			return "text/html";
		}

		/*--------------------------------------------------------------------------------------------
		### REMOVE JUNK ###
		--------------------------------------------------------------------------------------------*/
		private function remove_junk() {
			remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
			remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
			remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
			remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
			remove_action( 'wp_head', 'index_rel_link' ); // index link
			remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
			remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
			remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
			remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );

			remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
			remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
			remove_action( 'rest_api_init', 'wp_oembed_register_route' );// Remove the REST API endpoint.
			add_filter( 'embed_oembed_discover', '__return_false' );// Turn off oEmbed auto discovery.
			remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );// Don't filter oEmbed results.
			remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );// Remove oEmbed discovery links.
			remove_action( 'wp_head', 'wp_oembed_add_host_js' );// Remove oEmbed-specific JavaScript from the front-end and back-end.
		}

		/*--------------------------------------------------------------------------------------------
		### DEREGISTER WP-EMBED ###
		--------------------------------------------------------------------------------------------*/
		function deregister_wpembed() {
			if (!is_admin()) {
				wp_deregister_script('wp-embed');
			}
		}

		/*--------------------------------------------------------------------------------------------
		### REGISTER MAIN THEME MENU ###
		--------------------------------------------------------------------------------------------*/
		private function menus() {
			register_nav_menus( array(
				'primary'   => 'Main Menu',
			) );
		}

		/*--------------------------------------------------------------------------------------------
		### CUSTOM FILTERS ###
		--------------------------------------------------------------------------------------------*/
		//remove orphans and p tags, example: $text = apply_filters('remove_orphans', $text);
		function remove_orphans($content) {
			$remove = array(' w ',' z ',' o ',' i ',' a ', ' W ',' Z ',' O ',' I ',' A ');
			$insert = array(' w&nbsp;',' z&nbsp;',' o&nbsp;',' i&nbsp;',' a&nbsp;', ' W&nbsp;',' Z&nbsp;',' O&nbsp;',' I&nbsp;',' A&nbsp;');
			$content = str_replace($remove, $insert, $content);
			return $content;
		}

	}

endif;